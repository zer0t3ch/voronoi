#!/usr/bin/python
from algorithms import *
from Tkinter import *
from geo import *
import random


class Application(Frame):
	window_size = [300, 150]
	canvas_size = [200, 150]
	frames = ['', '', '']
	widgets = {}
	points = []
	lines = {
		'a': [],
		'c': [],
		'd': [],
		'v': []
	}
	toggles = {
		'c': False,
		'd': False,
		'v': False
	}
	canvas = None

	def loc(self, p):
		a = p.x * self.canvas_scale
		b = p.y * self.canvas_scale
		return a, b

	def create_frames(self):
		self.canvas_size = [self.window_size[0] - 100, self.window_size[1]]
		frame_one = self.frames[0] = Frame(
			self,
			width=self.canvas_size[0],
			height=self.canvas_size[1]
		)
		frame_two = self.frames[1] = Frame(
			self,
			width=100,
			height=self.window_size[1]
		)
		frame_three = self.frames[2] = Frame(
			self,
			height=self.window_size[1]
		)

		frame_one.pack(side='left', fill='both', expand=1)
		frame_two.pack(side='right', fill='y')
		frame_three.pack(side='right', fill='y')
		c = self.canvas = Canvas(frame_one)
		self.canvas.bind('<Configure>', self.on_resize)
		self.canvas.pack(fill='both', expand=1, anchor='nw')

	def on_resize(self, event):
		self.canvas_size = event.width, event.height
		self.regenerate_points()

	def clear_points(self):
		self.points = []

	def generate_points(self):
		for i in xrange(self.point_count.get()):
			s = self.canvas_scale
			w = round(self.canvas_size[0] / s)
			h = round(self.canvas_size[1] / s)
			wpad = round(w * self.point_padding, 0)
			hpad = round(h * self.point_padding, 0)
			randx = random.randint(wpad, w - wpad)
			randy = random.randint(hpad, h - hpad)
			p = Point(randx, randy)
			self.points.append(p)

	def draw_points(self):
		for p in self.points:
			a, b = self.loc(p)
			self.canvas.create_rectangle(
				a - self.dot_scale, b - self.dot_scale,
				a + self.dot_scale, b + self.dot_scale
			)

	def regenerate_points(self):
		self.clear_points()
		self.clear_lines()
		self.canvas.delete(ALL)
		self.generate_points()
		self.draw_points()
		self.calcs()

	def clear_lines(self, cat='a'):
		for l in self.lines[cat]:
			self.canvas.delete(l)
			del l

	def add_line(self, line, cat='a'):
		self.lines['a'].append(line)
		if cat != 'a':
			self.lines[cat].append(line)

	def calc_convex(self):
		for l in convex_lines(self.points):
			a, b, x, y = l.get_raw()
			a *= self.canvas_scale
			b *= self.canvas_scale
			x *= self.canvas_scale
			y *= self.canvas_scale
			temp_line = self.canvas.create_line(a, b, x, y)
			self.add_line(temp_line, cat='c')

	def calc_points(self, e):
		self.regenerate_points()

	def calcs(self):
		self.clear_lines()
		if self.convex_on.get() == 1:
			self.calc_convex()

	def create_buttons(self, parent):
		close = self.widgets['close'] = Button(
			parent,
			text='QUIT',
			command=self.quit,
			fg='red'
			)
		regen_points = self.widgets['regen_points'] = Button(
			parent,
			text='Regenerate Points',
			command=self.regenerate_points
			)
		convex = self.widgets['convex'] = Checkbutton(
			self.frames[1],
			text='Convex Hull',
			variable=self.convex_on,
			command=self.calcs
			)
		deluany = self.widgets['deluany'] = Checkbutton(
			self.frames[1],
			text='Deluany Triangulation',
			variable=self.deluany_on,
			command=self.calcs
			)
		voronoi = self.widgets['voronoi'] = Checkbutton(
			self.frames[1],
			text='Voronoi Diagram',
			variable=self.voronoi_on,
			command=self.calcs
			)
		point_scale = self.widgets['point_scale'] = Scale(
			self.frames[2],
			from_=0, to=200,
			variable=self.point_count,
			command=self.calc_points
			)
		point_scale.pack(fill='y', side='right')
		close.pack(fill='x', side='bottom')
		convex.pack(fill='x')
		deluany.pack(fill='x')
		voronoi.pack(fill='x')
		regen_points.pack(fill='x')

	def __init__(self, master):
		with open('config.py', 'r') as conf_file:
			conf = {}
			exec(conf_file.read(), conf)
			conf_file.close()
			self.point_count = IntVar(0)
			self.point_count.set(conf['point_count'])
			self.window_size = conf['window_size']
			self.canvas_scale = conf['canvas_scale']
			self.dot_scale = conf['dot_scale']
			self.point_padding = conf['point_padding']
		self.convex_on = IntVar(0)
		self.deluany_on = IntVar(0)
		self.voronoi_on = IntVar(0)
		self.convex_on.set(1)
		Frame.__init__(
			self, master,
			width=self.window_size[0],
			height=self.window_size[1]
		)
		self.pack(fill='both', expand=1)
		self.create_frames()
		self.create_buttons(self.frames[1])


window = Tk()
app = Application(window)
app.generate_points()
app.draw_points()
window.focus_set()
app.mainloop()
window.destroy()
