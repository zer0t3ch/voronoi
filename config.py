# Size of the window (in pixels)
window_size = [600, 300]


# Number of points to generate
point_count = 35


# Cuts down on the total number of possible points by forcing a specified
# amount of space between each point, virtually downscaling the canvas
canvas_scale = 1

# Controls the scale of dot drawing, purely asthetitc
dot_scale = 1

# Controls the padding on the edge of the canvas with a percentage
# of the canvas' size (0.025 adds a 2.5% x and y padding)
point_padding = 0.025
